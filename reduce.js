
function customReduce(array, reducer, initialValue) {
    let accumulator = initialValue === undefined ? array[0] : initialValue;
    let startIndex = initialValue === undefined ? 1 : 0;
  
    for (let index = startIndex; index < array.length; index++) {
      accumulator = reducer(accumulator, array[index], index, array);
    }
    return accumulator;
}

module.exports = customReduce;
  
  