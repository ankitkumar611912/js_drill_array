function square(element){
    return element*element;
}

function customMap(items,square){
    let squareItem = [];
    for(let index = 0 ; index < items.length ; index++){
        let resultant = square(items[index]);
        squareItem.push(resultant);
    }
    return squareItem;
}

module.exports = {
    square,
    customMap
}