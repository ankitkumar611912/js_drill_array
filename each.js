
function each(items,customForEach){
    for(let index = 0 ; index < items.length ; index++){
        customForEach(items[index],index);
    }
}

function customForEach(items,element,index){
    console.log(`Elements at index ${index} is ${element}`)
};

module.exports = {
    each,
    customForEach,
};