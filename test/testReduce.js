const items = [1,2,3,4,5,5];

const customReduce = require('../reduce');

console.log(`Total sum of items aray is ${customReduce(items,(acc, item) => acc + item, 0)}`);