function customFlat(array, depth ) {
    let result = [];
    array.forEach(item => {
      if (Array.isArray(item) && depth > 0) {
        // If the item is an array and depth is not 0, recursively flatten it
        result = result.concat(customFlat(item, depth - 1));
      } else {
        // Otherwise, add the item to the result array
        result.push(item);
      }
    });
    return result;
}
function findDepth(arr) {
    if (!Array.isArray(arr)) {
      return 0;
    }
    let depth = 0;
    arr.forEach(item => {
      if (Array.isArray(item)) {
        depth = Math.max(depth, findDepth(item));
      }
    });
  
    return depth + 1;
}

module.exports = {
    customFlat,
    findDepth
}
  

  