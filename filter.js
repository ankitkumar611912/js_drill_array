function customFilter(items){
    let oddArray = [];
    for(let index = 0 ; index < items.length ; index++){
        if(items[index] % 2 === 1)
        {
            oddArray.push(items[index]);
        }
    }
    return oddArray;
}

module.exports = customFilter;